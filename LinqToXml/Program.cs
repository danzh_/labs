﻿using System;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace LinqToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Выполнение запросов LINQ к XML-документу";

            CreateXmlDocument();

            XDocument xmldoc = XDocument.Load("CarDealer.xml");

            System.Console.WriteLine("........Результаты запросов LINQ к XML-документу........");

            string manufacturer = "Toyota";
            //1. Модели и цены автомобилей фирмы «Фирма».
            GetByManufacturer(xmldoc, manufacturer);

            string bodyType = "купе";
            //2. Средняя цена автомобиля с кузовом «Тип».
            GetAveragePriceByBodyType(xmldoc, bodyType);

            int power = 200;
            //3. Данные по автомобилям мощностью более «», сгрупп по фирмам. 
            GetByPowerGroupByManufacturer(xmldoc, power);

            //4. Список автомобилей с указанием ответств. сотрудника (join).
            GetByResponsibleEmployee(xmldoc);

            int power2 = 200;
            //5. Все автомобили с мощностью двигателя более «...» (XPath).
            GetByPowerXPath(xmldoc, power2);
        }

        private static void CreateXmlDocument()
        {
            var doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("автодилер",
                    new XElement("машина", new XAttribute("номер", 1),
                        new XElement("модель", "Mark2"),
                        new XElement("фирма", "Toyota"),
                        new XElement("цена", 800000),
                        new XElement("кузов", "седан"),
                        new XElement("ответственный_работник", "Анатолий"),
                        new XElement("мощность", "200")
                    ),
                    new XElement("машина", new XAttribute("номер", 2),
                        new XElement("модель", "A80"),
                        new XElement("фирма", "Toyota"),
                        new XElement("цена", 2400000),
                        new XElement("кузов", "купе"),
                        new XElement("ответственный_работник", "Дмитрий"),
                        new XElement("мощность", "300")
                    ),
                    new XElement("машина", new XAttribute("номер", 3),
                        new XElement("модель", "GT-R"),
                        new XElement("фирма", "Nissan"),
                        new XElement("цена", 4600000),
                        new XElement("кузов", "купе"),
                        new XElement("ответственный_работник", "Анатолий"),
                        new XElement("мощность", "550")
                    ),
                    new XElement("машина", new XAttribute("номер", 4),
                        new XElement("модель", "Civic"),
                        new XElement("фирма", "Honda"),
                        new XElement("цена", 900000),
                        new XElement("кузов", "хетчбек"),
                        new XElement("ответственный_работник", "Дмитрий"),
                        new XElement("мощность", "150")
                    ),
                    new XElement("машина", new XAttribute("номер", 5),
                        new XElement("модель", "RX-7"),
                        new XElement("фирма", "Mazda"),
                        new XElement("цена", 2400000),
                        new XElement("кузов", "купе"),
                        new XElement("ответственный_работник", "Вячеслав"),
                        new XElement("мощность", "300")
                    )
                )
            );

            doc.Save("CarDealer.xml");
        }

        private static void GetByManufacturer(XDocument xmldoc, string manufacturer)
        {
            var result = from car in xmldoc.Descendants("машина")
                        where (string)car.Element("фирма") == manufacturer
                        select car;

            System.Console.WriteLine($"........Машины фирмы {manufacturer}........");

            foreach (var car in result)
            {
                System.Console.WriteLine((string)car.Element("модель"));
            }
        }

        private static void GetAveragePriceByBodyType(XDocument xmldoc, string bodyType)
        {
            var result = (from car in xmldoc.Descendants("машина")
                        where (string)car.Element("кузов") == bodyType
                        select Convert.ToDouble(car.Element("цена").Value)).Average();

            System.Console.WriteLine($"........Средняя стоимость машин с кузовом типа {bodyType}........");

            System.Console.WriteLine(result);
        }

        private static void GetByPowerGroupByManufacturer(XDocument xmldoc, int power)
        {
            var groups = from car in xmldoc.Descendants("машина")
                        where (int)car.Element("мощность") > power
                        group car by car.Element("фирма");

            System.Console.WriteLine($"........Машины мощности выше {power}........");

            foreach (var group in groups)
            {
                System.Console.WriteLine($"Машины {(string)group.Key} мощностью выше {power}");

                foreach (var car in group)
                {
                    System.Console.WriteLine((string)car.Element("модель"));
                }
            }
        }

        private static void GetByResponsibleEmployee(XDocument xmldoc)
        {
            XElement employees = new XElement("работники_салона",
                new XElement("работник",  new XAttribute("код", "а111"),
                    new XAttribute("номер_машины", 1),
                    new XElement("фио", "Буров Анатолий Григорьевич")
                ),
                new XElement("работник",  new XAttribute("код", "а112"),
                    new XAttribute("номер_машины", 2),
                    new XElement("фио", "Петров Дмитрий Александрович")
                ),
                new XElement("работник",  new XAttribute("код", "а113"),
                    new XAttribute("номер_машины", 3),
                    new XElement("фио", "Кудрова Татьяна Михайловна")
                ),
                new XElement("работник",  new XAttribute("код", "а114"),
                    new XAttribute("номер_машины", 4),
                    new XElement("фио", "Самойлов Константин Михайлович")
                ),
                new XElement("работник",  new XAttribute("код", "а115"),
                    new XAttribute("номер_машины", 5),
                    new XElement("фио", "Иванов Михаил Дмитриевич")
                )
            );

            var result = from car in xmldoc.Descendants("машина")
                        from model in car.Elements("модель")
                        join emp in employees.Elements("работник")
                            on car.Attribute("номер").Value equals emp.Attribute("номер_машины").Value
                        select new {
                            name = emp.Element("фио").Value,
                            carMan = car.Element("фирма").Value,
                            carMod = car.Element("модель").Value
                        };

            System.Console.WriteLine($"........Работники автодилера и машины, за которые они ответственны........");

            foreach (var r in result)
            {
                System.Console.WriteLine($"{r.name}\t - {r.carMan} {r.carMod}");
            }
        }

        private static void GetByPowerXPath(XDocument xmldoc, int power)
        {
            var result = xmldoc.XPathSelectElements($"//машина[мощность>{power}]");

            System.Console.WriteLine($"........Машины мощности выше {power}(XPath)........");
            foreach (var car in result)
            {
                System.Console.WriteLine($"{car.Element("фирма").Value} {car.Element("модель").Value}");
            }
        }
    }
}
